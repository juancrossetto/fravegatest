﻿using Fravega.Api.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Fravega.Api.Core.Domain.Mappings
{
    public class CajaConfiguration : IEntityTypeConfiguration<Caja>
    {

        public void Configure(EntityTypeBuilder<Caja> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Descripcion);
        }   
    }

}
