﻿using Fravega.Api.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Fravega.Api.Core.Domain.Mappings
{
    public class TipoCajaConfiguration : IEntityTypeConfiguration<TipoCaja>
    {
        public void Configure(EntityTypeBuilder<TipoCaja> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Descripcion);
            builder.HasMany(x => x.Cajas).WithOne(b => b.TipoCaja).HasForeignKey(b=> b.TipoCajaId);
        }
    }
}
