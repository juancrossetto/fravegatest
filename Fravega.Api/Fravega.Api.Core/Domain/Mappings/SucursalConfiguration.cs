﻿using Fravega.Api.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Fravega.Api.Core.Domain.Mappings
{
    public class SucursalConfiguration : IEntityTypeConfiguration<Sucursal>
    {
        public void Configure(EntityTypeBuilder<Sucursal> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Direccion);
            builder.Property(x => x.Telefono);
            builder.HasMany(x => x.Cajas).WithOne(b => b.Sucursal).HasForeignKey(b => b.SucursalId);
        }
    }
}
