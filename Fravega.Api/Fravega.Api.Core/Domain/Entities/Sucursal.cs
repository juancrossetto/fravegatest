﻿using Fravega.Api.Core.Shared;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fravega.Api.Core.Domain.Entities
{
    [Table("Sucursales")]
    public class Sucursal : BaseEntity<int>
    {

        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public ICollection<Caja> Cajas
        {
            get => _lazyLoader.Load(this, ref _cajas);
            set => _cajas = value;
        }

        //[ForeignKey("Id")]
        //public Guid IdCaja { get; set; }
        //private Caja _caja;
        //public Caja Caja
        //{
        //    get => _lazyLoader.Load(this, ref _caja);
        //    set => _caja = value;
        //}


        #region Private Properties
        private readonly ILazyLoader _lazyLoader;
        private ICollection<Caja> _cajas;
        #endregion

        #region Constructors
        public Sucursal(ILazyLoader lazyLoader)
        {
            _lazyLoader = lazyLoader;
        }

        public Sucursal(int id, string direccion, string telefono)
        {
            Direccion = direccion;
            Telefono = telefono;
        }
        #endregion
    }
}
