﻿using Fravega.Api.Core.Shared;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fravega.Api.Core.Domain.Entities
{
    [Table("Cajas")]
    public class Caja : BaseEntity<Guid>
    {

        [ForeignKey("Id")]
        public int SucursalId { get; set; }
        public Sucursal Sucursal
        {
            get => _lazyLoader.Load(this, ref _sucursal);
            set => _sucursal = value;
        }

        public string Descripcion { get; set; }


        [ForeignKey("Id")]
        public int TipoCajaId { get; set; }
        public TipoCaja TipoCaja
        {
            get => _lazyLoader.Load(this, ref _tipoCaja);
            set => _tipoCaja = value;
        }

        #region private Properties
        private readonly ILazyLoader _lazyLoader;
        private Sucursal _sucursal;
        private TipoCaja _tipoCaja;
        #endregion

        #region constructors
        public Caja(ILazyLoader lazyLoader)
        {
            _lazyLoader = lazyLoader;
        }

        public Caja(Guid id, string descripcion, Sucursal sucursal, TipoCaja tipoCaja)
        {
            Id = id;
            Descripcion = descripcion;
            Sucursal = sucursal;
            TipoCaja = tipoCaja;
        }

        #endregion
    }
}
