﻿using Fravega.Api.Core.Shared;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Fravega.Api.Core.Domain.Entities
{
    [Table("TiposCaja")]
    public class TipoCaja : BaseEntity<int>
    {

        public string Descripcion { get; set; }
        public ICollection<Caja> Cajas
        {
            get => _lazyLoader.Load(this, ref _cajas);
            set => _cajas = value;
        }

        #region Private Properties
        private readonly ILazyLoader _lazyLoader;
        private ICollection<Caja> _cajas;
        #endregion

        #region Constructors
        public TipoCaja(ILazyLoader lazyLoader)
        {
            _lazyLoader = lazyLoader;
        }

        public TipoCaja(int id, string descripcion)
        {
            Id = id;
            Descripcion = descripcion;
        }

        #endregion
    }
}
