﻿using Fravega.Api.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fravega.Api.Infrastructure.Data.Repositories.Interfaces
{
    public interface ICajaRepository : IBaseRepository<Caja,Guid> 
    {
        //Implementar metodos propios de caja
        Task<IEnumerable<Caja>> GetAllBySucursalAsync(int sucursalId);
    }
}
