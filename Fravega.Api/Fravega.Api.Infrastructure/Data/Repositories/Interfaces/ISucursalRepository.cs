﻿using Fravega.Api.Core.Domain.Entities;

namespace Fravega.Api.Infrastructure.Data.Repositories.Interfaces
{
    public interface ISucursalRepository : IBaseRepository<Sucursal, int>
    {
        //Implementar metodos propios de sucursal
    }
}
