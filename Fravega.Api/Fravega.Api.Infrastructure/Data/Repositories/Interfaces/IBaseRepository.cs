﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fravega.Api.Infrastructure.Data.Repositories.Interfaces
{
    public interface IBaseRepository<TEntity, TId>  where TEntity : class
    {
        Task<IEnumerable<TEntity>> GetAllAsync();

        Task<TEntity> GetOneAsync(TId id);

        Task AddAsync(TEntity entity);
    }
}
