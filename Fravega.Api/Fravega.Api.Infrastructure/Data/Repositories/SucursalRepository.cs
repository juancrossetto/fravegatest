﻿using Fravega.Api.Core.Domain.Entities;
using Fravega.Api.Infrastructure.Data.Repositories.Interfaces;

namespace Fravega.Api.Infrastructure.Data.Repositories
{
    public class SucursalRepository : BaseRepository<Sucursal,int>, ISucursalRepository
    {

        public SucursalRepository(AppDbContext db) : base(db) { }

    }
}
