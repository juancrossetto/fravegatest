﻿using Fravega.Api.Core.Domain.Entities;
using Fravega.Api.Infrastructure.Data.Repositories.Interfaces;
using LazyCache;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fravega.Api.Infrastructure.Data.Repositories
{
    public class CajaRepository : BaseRepository<Caja, Guid>, ICajaRepository
    {
        public CajaRepository(AppDbContext db) : base(db) { }

        public async Task<IEnumerable<Caja>> GetAllBySucursalAsync(int sucursalId)
        {
            var _cache = new CachingService();
            string keyWithSucursalId = CacheKey + "_sucursalId_" + sucursalId.ToString();
            var result = await _cache.GetOrAddAsync(keyWithSucursalId, async () =>
            {
                IEnumerable<Caja> cajas = await _db.Cajas
                                .Where(x => x.SucursalId == sucursalId)
                                .ToListAsync();
                return Task.FromResult(cajas);
            });
            return result.Result;
        }
    }
}
