﻿using Fravega.Api.Core.Shared;
using Fravega.Api.Infrastructure.Data.Repositories.Interfaces;
using LazyCache;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fravega.Api.Infrastructure.Data.Repositories
{
    public class BaseRepository<TEntity,TId> : IBaseRepository<TEntity,TId> where TEntity : BaseEntity<TId>
    {
        protected static string CacheKey = typeof(TEntity).Name.ToString();
        protected readonly AppDbContext _db;
        public BaseRepository(AppDbContext db) => _db = db;
           
        
        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            var _cache = new CachingService();
            var result = await _cache.GetOrAddAsync(CacheKey, async () =>
            {
                IEnumerable<TEntity> entity = await _db.Set<TEntity>().ToListAsync();
                return  Task.FromResult(entity);
            });
            return result.Result;

        }
            

        public async Task<TEntity> GetOneAsync(TId id)
        {
            var _cache = new CachingService();
            string keyWithId = CacheKey + "_id_" + id.ToString();
            var result = await _cache.GetOrAddAsync(keyWithId, async () =>
            {
                TEntity entity = await _db.Set<TEntity>().SingleOrDefaultAsync(x => x.Id.Equals(id));
                return Task.FromResult(entity);
            });
            return result.Result;
        } 

        public async Task AddAsync(TEntity entity)
        {
            var _cache = new CachingService();
            var result = await _cache.GetOrAddAsync(CacheKey + "_Insert", async () =>
            {
                var entitySaved = await _db.Set<TEntity>().AddAsync(entity);
                await _db.SaveChangesAsync();
                return Task.FromResult(entitySaved.Entity);
            });
         
        }

    }
}
