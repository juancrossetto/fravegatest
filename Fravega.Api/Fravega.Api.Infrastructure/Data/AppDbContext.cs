﻿using Fravega.Api.Core.Domain.Entities;
using Fravega.Api.Core.Domain.Mappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace Fravega.Api.Infrastructure.Data
{
    public class AppDbContext : DbContext
    {

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }
        public DbSet<Caja> Cajas { get; set; }
        public DbSet<Sucursal> Sucursales { get; set; }
        public DbSet<TipoCaja> TiposCaja { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.ConfigureWarnings(warnings =>
             warnings.Default(WarningBehavior.Ignore)
                     .Log(CoreEventId.IncludeIgnoredWarning, CoreEventId.LazyLoadOnDisposedContextWarning)
                     .Throw(RelationalEventId.QueryClientEvaluationWarning));
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CajaConfiguration());
            builder.ApplyConfiguration(new SucursalConfiguration());
            builder.ApplyConfiguration(new TipoCajaConfiguration());

            base.OnModelCreating(builder);
        }
    }
}
