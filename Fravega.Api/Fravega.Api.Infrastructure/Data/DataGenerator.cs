﻿using Fravega.Api.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace Fravega.Api.Infrastructure.Data
{
    public class DataGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new AppDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<AppDbContext>>()))
            {
                if (context.Cajas.Any())
                    return;   // Data was already seeded


                int cantSucursales = 20;
                TipoCaja cajaChica = new TipoCaja(2,"Caja Chica");
                TipoCaja cajaGrande = new TipoCaja(3, "Caja Grande");
                bool esCajaChica = true;
                for (int i = 1; i <= cantSucursales; i++)
                {
                    TipoCaja tipoCaja = esCajaChica ? cajaChica : cajaGrande;
                    esCajaChica = !esCajaChica;

                    var sucursal = new Sucursal(i, $"Direccion {i}", $"424242{i}");
                 

                    context.TiposCaja.Add(tipoCaja);

                    context.Sucursales.Add(sucursal);
                    for (int j = 0; j < 5; j++)
                    {
                        context.Cajas.Add(
                           new Caja(Guid.NewGuid(), $"Caja Numero {j}", sucursal, tipoCaja));
                    }
                    

                }

                context.SaveChanges();
            }
        }
    }
}
