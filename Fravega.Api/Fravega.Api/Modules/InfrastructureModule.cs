﻿using Autofac;
using Fravega.Api.Core.Shared;
using Fravega.Api.Infrastructure.Data.Repositories;
using Fravega.Api.Infrastructure.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fravega.Api.Modules
{
    public class InfrastructureModule : Module
    {
      
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CajaRepository>()
                .As<ICajaRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<SucursalRepository>()
                .As<ISucursalRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(IBaseRepository<,>))
                .As(typeof(IBaseRepository<,>))
                .InstancePerLifetimeScope();

          
        }
    }
}
