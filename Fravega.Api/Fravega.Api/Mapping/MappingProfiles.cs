﻿using AutoMapper;
using Fravega.Api.ApiModels;
using Fravega.Api.Core.Domain.Entities;
using Fravega.Api.Core.Shared;
using System;
using System.Collections.Generic;

namespace Fravega.Api.Mapping
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Caja, CajaDTO>().ReverseMap();
            CreateMap<TipoCaja, TipoCajaDTO>().ReverseMap();
            CreateMap<Sucursal, SucursalDTO>().ReverseMap();
        }
    }
}
