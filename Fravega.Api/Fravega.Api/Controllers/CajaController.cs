﻿using AutoMapper;
using Fravega.Api.ApiModels;
using Fravega.Api.Core.Domain.Entities;
using Fravega.Api.Infrastructure.Data.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fravega.Api.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class CajaController : Controller
    {
        private readonly ICajaRepository _cajaRepository;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="cajaRepository">Caja Repository</param>
        /// <param name="mapper">AutoMapper</param>
        public CajaController(ICajaRepository cajaRepository,IMapper mapper)
        {
            _cajaRepository = cajaRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Devuelve todas las cajas existentes.
        /// </summary>
        /// <returns>Listado de cajas.</returns>
        [HttpGet]
        public async Task<IEnumerable<CajaDTO>> ListAsync()
        {
            var cajas = await this._cajaRepository.GetAllAsync();
            List<CajaDTO> result = _mapper.Map<List<Caja>, List<CajaDTO>>(cajas.ToList());
            return result;
        }

        /// <summary>
        /// Devuelve una caja existente.
        /// </summary>
        /// <returns>Caja con Id enviado por parametro</returns>
        //[HttpGet("{id}")]
        [HttpGet]
        [Route("{id:Guid}")]
        public async Task<CajaDTO> ListByIdAsync(Guid id)
        {
            var caja = await this._cajaRepository.GetOneAsync(id);
            CajaDTO result = _mapper.Map<Caja, CajaDTO>(caja);
            return result;
        }

        /// <summary>
        /// Devuelve una lista de cajas pertenecientes a una sucursal.
        /// </summary>
        /// <returns>Cajas con Sucursal cuyo Id es enviado por parametro</returns>
        [HttpGet]
        [Route("{sucursalId:int}")]
        public async Task<IEnumerable<CajaDTO>> ListBySucursalIdAsync(int sucursalId)
        {
            List<CajaDTO> result;
            var cajas = await this._cajaRepository.GetAllBySucursalAsync(sucursalId);
            result = _mapper.Map<List<Caja>, List<CajaDTO>>(cajas.ToList());
            return result;
        }

        /// <summary>
        /// Guarda nueva Caja
        /// </summary>
        /// <param name="caja">Caja</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] CajaDTO caja)
        {
            Caja cajaToPersist;
            CajaDTO result;
            try
            {
                cajaToPersist = _mapper.Map<CajaDTO, Caja>(caja);
                await this._cajaRepository.AddAsync(cajaToPersist);
                result = _mapper.Map<Caja, CajaDTO>(cajaToPersist);
            }
            catch (Exception ex)
            {
                return BadRequest($"Error:{ex.Message}");
            }
            return Ok(result);
        }
    }
}