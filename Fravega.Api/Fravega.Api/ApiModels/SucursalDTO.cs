﻿namespace Fravega.Api.ApiModels
{
    public class SucursalDTO
    {
        public int Id { get; }
        public string Direccion { get; }
        public string Telefono { get; }

        public SucursalDTO(int id, string direccion, string telefono)
        {
            Id = id;
            Direccion = direccion;
            Telefono = telefono;
        }
    }
}
