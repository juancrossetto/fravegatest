﻿using System;

namespace Fravega.Api.ApiModels
{
    public class CajaDTO 
    {
        public Guid Id { get; }
        public SucursalDTO Sucursal { get; }
        public string Descripcion { get; }
        public TipoCajaDTO TipoCaja { get; }

        public CajaDTO(Guid id, SucursalDTO sucursal, string descripcion, TipoCajaDTO tipoCaja) 
        {
            Id = id;
            Sucursal = sucursal;
            Descripcion = descripcion;
            TipoCaja = tipoCaja;
        }
    }
}
