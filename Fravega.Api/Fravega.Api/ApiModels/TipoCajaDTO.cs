﻿namespace Fravega.Api.ApiModels
{
    public class TipoCajaDTO
    {
        public int Id { get; }
        public string Descripcion { get; }

        public TipoCajaDTO(int id, string descripcion) 
        {
            Id = id;
            Descripcion = descripcion;
        }
    }
}
